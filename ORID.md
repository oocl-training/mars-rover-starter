## ORID

### O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
1. TDD
2. Let others review my code. Complete Command Pattern in my homework.
3. Code Review
```
### R (Reflective)
Please use one word to express your feelings about today's class.
```
full
```
### I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
1. Test drive Development.
    It can help me find bugs.
    After multiple iterations of the code, it can help me ensure that the previous code functionality remains unchanged.
2. Code Review help me change the bad programming style.
```
### D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
Faced with a larger requirement, during the code implementation process, it is likely to disrupt the functionality of the previous code. We can use the method of TDD, when we commit once, run all test case once.
```