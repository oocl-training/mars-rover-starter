package com.afs.tdd;


import com.afs.tdd.command.Command;
import com.afs.tdd.command.CommandMove;
import com.afs.tdd.command.CommandTurnLeft;
import com.afs.tdd.command.CommandTurnRight;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandMove();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_negative_1_South_when_executeCommand_given_location_0_0_South_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandMove();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_East_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandMove();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_negative_1_0_West_when_executeCommand_given_location_0_0_West_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandMove();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_turn_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnLeft();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_turn_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnLeft();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_turn_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnLeft();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_turn_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnLeft();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_North_and_command_turn_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnRight();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_turn_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnRight();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_turn_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnRight();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_turn_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = new CommandTurnRight();
        // when
        Location locationAfterMove = marsRover.executeCommand(command);
        // then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_1_South_when_executeCommand_given_location_0_0_North_and_batch_commands_MLMRMRMMRMM() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> batchCommands = new LinkedList<>();
        Command commandMove = new CommandMove();
        Command commandTurnLeft = new CommandTurnLeft();
        Command commandTurnRight = new CommandTurnRight();
        batchCommands.add(commandMove);
        batchCommands.add(commandTurnLeft);
        batchCommands.add(commandMove);
        batchCommands.add(commandTurnRight);
        batchCommands.add(commandMove);
        batchCommands.add(commandTurnRight);
        batchCommands.add(commandMove);
        batchCommands.add(commandMove);
        batchCommands.add(commandTurnRight);
        batchCommands.add(commandMove);
        batchCommands.add(commandMove);
        // when
        Location locationAfterMove = marsRover.executeBatchCommands(batchCommands);
        // then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }
}
