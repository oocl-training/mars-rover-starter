package com.afs.tdd.command;

import com.afs.tdd.Location;

public class CommandMove implements Command{

    @Override
    public void execute(Location location) {
        switch (location.getDirection()){
            case North:
                location.setCoordinateY(location.getCoordinateY()+1);
                break;
            case South:
                location.setCoordinateY(location.getCoordinateY()-1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX()+1);
                break;
            case West:
                location.setCoordinateX(location.getCoordinateX()-1);
                break;
        }
    }
}
