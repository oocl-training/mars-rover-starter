package com.afs.tdd.command;

import com.afs.tdd.Direction;
import com.afs.tdd.Location;

public class CommandTurnLeft implements Command {
    @Override
    public void execute(Location location) {
        switch (location.getDirection()){
            case North:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
        }
    }
}
