package com.afs.tdd.command;

import com.afs.tdd.Direction;
import com.afs.tdd.Location;

import java.util.List;

public class CommandTurnRight implements Command{
    @Override
    public void execute(Location location) {
        switch (location.getDirection()){
            case North:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
    }
}
