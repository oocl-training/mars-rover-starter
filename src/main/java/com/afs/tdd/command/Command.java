package com.afs.tdd.command;

import com.afs.tdd.Location;

public interface Command {
    void execute(Location location);
}
