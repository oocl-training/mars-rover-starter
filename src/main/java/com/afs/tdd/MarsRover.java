package com.afs.tdd;

import com.afs.tdd.command.Command;
import com.afs.tdd.command.CommandMove;
import com.afs.tdd.command.CommandTurnLeft;
import com.afs.tdd.command.CommandTurnRight;

import java.util.List;

public class MarsRover {
    // Invoker, caller of Command Pattern.
    // Receive commands and calling them.
    // It is the Subject when executing commands.
    Location location;


    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command command) {
        command.execute(location);
        return location;
    }

    public Location executeBatchCommands(List<Command> commands) {
        commands.forEach(this::executeCommand);
        return location;
    }
}
